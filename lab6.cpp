/*
********************************************
 * Elena Le
 * CPSC 1021, Lab Section: 003, F20
 * eule@clemson.edu
 * Elliot McMillan / Victoria Xu
********************************************
 */

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee inputEmployee[10]; //array of 10 employees

	for (int i = 0; i < 10; i++) {
	std::cout << "Enter last name of employee " << i+1 << " :" << endl;
	std::cin >> inputEmployee[i].lastName;

	std::cout << "Enter first name of employee " << i+1 << " :" <<  endl;
	std::cin >> inputEmployee[i].firstName;

	std::cout << "Enter year of birth of employee " << i+1 << " :" <<  endl;
	std::cin >> inputEmployee[i].birthYear;

	std::cout << "Enter hourly wage of employee " << i+1 << " :" <<  endl;
	std::cin >> inputEmployee[i].hourlyWage;
	std::cout << '\n';

	}
	std::cout << '\n';

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

	 random_shuffle(&inputEmployee[0], &inputEmployee[10], myrandom); //shuffle all 10 employee

   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
		employee smallerarray[5]; //smaller array size

		for (int i =0; i < 5; i++) { //grab the first 5 employee and put them in a smaller array
			smallerarray[i] = inputEmployee[i];
		}

    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 sort(&smallerarray[0], &smallerarray[5], name_order); //sorts the smaller array

    /*Now print the array below */

		for (employee i : smallerarray) { //prints array with right alignment
			std::cout << setw(20) << right << i.lastName +", " + i.firstName << endl;
			std::cout << setw(20) << right << i.birthYear << endl;
			std::cout << setw(20) << right<<fixed<<setprecision(2)<<i.hourlyWage<<endl;
			std::cout << '\n';
		}
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT

	if (lhs.lastName < rhs.lastName) { //orders the lastName
		return true;
	}

	else {
		return false;
	}
}
